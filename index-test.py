#!/usr/bin/python3
# Crypto assert: cryptographically based assertions with compile time SHA-256
# using C++11 constexpr.
#
# Copyright (c) 2016 Matthew Ready
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
import unittest
import index

SIMPLE_TEXT = """
#include  <iostream>

int main() {
    CRYPTO_ASSERT(arg0);
}
"""

MANY_ARGS_WITH_BRACKETS = "CRYPTO_ASSERT(arg0, (a), (),\"\\\")\",\")\");"

MANY_ARGS_WITH_BRACKETS_DEFINE = \
    "#define CRYPTO_ASSERT(arg0, (a), (),\"\\\")\",\")\");"

LINE_COMMENTED = "CRYPTO_2_ASSERT(arg0, a, b);\n//CRYPTO_ASSERT(arg0, a, b);\n"

BLOCK_COMMENTED = """
#include  <iostream>
/*
int main() {
    CRYPTO_2_ASSERT(arg0);*/
    /*CRYPTO_2_ASSERT(arg0);*/
    CRYPTO_2_ASSERT(arg0);
}
"""

RAW_STRINGED = """
#include  <iostream>
/*
int main() {*/
R"TEST(
    CRYPTO_ASSERT(arg0);
    )TEST"
}
"""

STRINGED = "\"CRYPTO_ASSERT(arg0, a, b);\"\nCRYPTO_ASSERT(arg0, a, b);\n"

HMAC_KEY = (b"\x00" * 64)


class IndexTestCase(unittest.TestCase):
    def _make_plain(self, x):
        return (str(x[0]) + " " + x[1] + " " + x[2]).encode()

    def _test(self, source, expected):
        result = index.scan_source(source, "test.cpp", ["CRYPTO_ASSERT", "CRYPTO_2_ASSERT"],
                                   HMAC_KEY)

        keys = ["line_number", "basename", "assertion_expression",
                "hmac", "hmac_plain_text"]
        expected_long_form = [
            {
                k: v for k, v in
                zip(keys, x + [self._make_plain(x)])
            }
            for x in expected
        ]

        self.assertEqual(expected_long_form, result)

    def test_basic(self):
        self._test(SIMPLE_TEXT, [[5, 'test.cpp', 'arg0', "f061c2b1c96d99d2ea0b83ef113bbeba13ba5dd16b7d528416eab3592f1249a6"]])

    def test_many_args_with_brackets(self):
        self._test(MANY_ARGS_WITH_BRACKETS, [[1, 'test.cpp', 'arg0', "aff4b6be47f823f1968c5e4bf4c1d06cf22b827f79aa21c3d1d8e6b6f5ca74fa"]])

    def test_many_args_with_brackets_define(self):
        self._test(MANY_ARGS_WITH_BRACKETS_DEFINE, [])

    def test_line_commented(self):
        self._test(LINE_COMMENTED, [[1, 'test.cpp', 'arg0', "aff4b6be47f823f1968c5e4bf4c1d06cf22b827f79aa21c3d1d8e6b6f5ca74fa"]])

    def test_block_commented(self):
        self._test(BLOCK_COMMENTED, [[7, 'test.cpp', 'arg0', "a7d192598471899283e2c0ccec2ab16d682373b85871bfe7171af735bc456bb4"]])

    def test_raw_stringed(self):
        self._test(RAW_STRINGED, [])

    def test_stringed(self):
        self._test(STRINGED, [[2, 'test.cpp', 'arg0', "baec86eb17bb112a6a4055eb5c1d69e6b2afc4ad33a38149597cb7d544f777ca"]])


if __name__ == '__main__':
    unittest.main()

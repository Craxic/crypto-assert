/**
 * Crypto assert: cryptographically based assertions with compile time SHA-256 using C++11 constexpr.
 *
 *     Copyright (c) 2016 Matthew Ready
 *
 *     Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *     documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 *     the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 *     to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *     The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 *     the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 *     THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *     CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *     IN THE SOFTWARE.
 */

#include "CryptoAssert.h"
#include <iostream>

// Generated with:
// < /dev/urandom tr -dc a-f0-9 | head -c128 - | xargs python -c "import sys; a=sys.argv[1]; print(\", \".join([\"0x\" + a[i*2] + a[i*2+1] for i in range(len(a)/2)]));" | fold -s -w 48
// Verify with:
// echo -n "31 main.cpp 0" | openssl dgst -sha256 -hmac "`echo -e "\xc2\x23\x43\xf0\x26\x8d\x85\xee\x2b\x4d\xd4\xe0\x16\x86\x77\xd7\xff\xe9\x29\xfa\x52\x60\xc2\xf1\x32\x04\x86\x92\x19\xb1\xdf\xed\x0e\xb4\x5f\xfc\xb5\x17\x3f\x39\xc9\x9e\x11\xf9\x39\xb1\x2e\x9e\x81\x6d\x08\x8c\x2c\x83\x3b\x84\x96\x81\x37\xa4\xfe\xbb\xe1\xc6"`"
constexpr uint8_t CRYPTO_ASSERT_KEY_RAW[64] = {
    0xc2, 0x23, 0x43, 0xf0, 0x26, 0x8d, 0x85, 0xee, 
    0x2b, 0x4d, 0xd4, 0xe0, 0x16, 0x86, 0x77, 0xd7, 
    0xff, 0xe9, 0x29, 0xfa, 0x52, 0x60, 0xc2, 0xf1, 
    0x32, 0x04, 0x86, 0x92, 0x19, 0xb1, 0xdf, 0xed, 
    0x0e, 0xb4, 0x5f, 0xfc, 0xb5, 0x17, 0x3f, 0x39, 
    0xc9, 0x9e, 0x11, 0xf9, 0x39, 0xb1, 0x2e, 0x9e, 
    0x81, 0x6d, 0x08, 0x8c, 0x2c, 0x83, 0x3b, 0x84, 
    0x96, 0x81, 0x37, 0xa4, 0xfe, 0xbb, 0xe1, 0xc6
};

constexpr constexpr_sha256::HmacKey CRYPTO_ASSERT_KEY = 
    constexpr_sha256::HmacKey::Construct(CRYPTO_ASSERT_KEY_RAW, sizeof(CRYPTO_ASSERT_KEY_RAW));

// Example assertion macro. You can put whatever you like :)
#define CRYPTO_ASSERT(expression, extras...) { \
    if (!(expression)) { \
        constexpr auto CRYPTO_ASSERT_HASH_STRING = crypto_assert::CryptoAssertGetHashString(CRYPTO_ASSERT_KEY, __LINE__, __FILE__, #expression ); \
        auto CRYPTO_ASSERT_STRING = crypto_assert::ConcatStrings(CRYPTO_ASSERT_HASH_STRING.string, extras); \
        std::cout << "Assertion failed: " << CRYPTO_ASSERT_STRING << std::endl; \
    } \
}

int main() {
    // Prints "Assertion failed: c4005a14a8b5180738c4c822d1d020ad65c088ac7648610be5ef00d6cc58216f You can put important values here like 42 and 42.230000 and they'll appear in the assertion string" (hash of "33 main.cpp 0")
    CRYPTO_ASSERT(0, "You", "can", "put", "important", "values", "here", "like", 42, "and", 42.23, "and", "they'll appear in the assertion string");
}

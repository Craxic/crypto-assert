# C++11 Crypto Assert
## What is this?
This allows you to create assertions that don't leave any traces of the expressions, files or line 
numbers involved. This is good not only for reducing size of the resulting binary, but also for the
security of your codebase. Instead of your assertions printing the above data, it will print a 
SHA256 based HMAC that is calculated at compile-time. Note that this library only provides you the 
means to create this hash at compile-time, not any actual assertion features.
## Compatibility
Tested on GCC 4.8.4 and Clang 3.5.0.
## License
MIT. Please see the source for the full license.

/**
 * Crypto assert: cryptographically based assertions with compile time SHA-256 using C++11 constexpr.
 *
 *     Copyright (c) 2016 Matthew Ready
 *
 *     Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *     documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 *     the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 *     to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *     The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 *     the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 *     THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *     CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *     IN THE SOFTWARE.
 */

#ifndef CRYPTO_ASSERT_H
#define CRYPTO_ASSERT_H

#include "ConstexprSha256.h"
#include "stdint.h"
#include "stddef.h"
#include <string>
#include <sstream>
#include <type_traits>

namespace crypto_assert {

namespace detail {

// The indices trick from http://loungecpp.wikidot.com/tips-and-tricks%3aindices
// Really, really useful when making compile-time arrays.
template <size_t... Is>
struct indices {};

template <size_t N, size_t... Is>
struct build_indices
        : build_indices<N-1, N-1, Is...> {};

template <size_t... Is>
struct build_indices<0, Is...> : indices<Is...> {};

template<size_t N>
struct HeadPushString {
    char string[N] = { 0 };
    size_t strlen = 0;
    
    constexpr HeadPushString() {}
    
private:
    template<size_t... Is>
    constexpr HeadPushString(indices<Is...>, const char (&string)[N], size_t strlen) : 
        string { string[Is]... }, strlen(strlen) {}
    
    template<size_t... Is>
    constexpr HeadPushString PushCharFront(indices<Is...>, char c) const {
        return HeadPushString(build_indices<N>(), { c, string[Is]... }, strlen+1);
    }

public:
    constexpr HeadPushString PushCharFront(char c) const {
        return PushCharFront(build_indices<N-1>(), c);
    }
};

template<typename T>
constexpr char MakeDigit(const T & integer) {
    return "0123456789"[integer % 10];
}

template<size_t N, typename T>
constexpr HeadPushString<N> UIntToString(const HeadPushString<N> & accumulator, const T & integer) {
    return integer <= 9 ? accumulator.PushCharFront(MakeDigit(integer)) : UIntToString(accumulator.PushCharFront(MakeDigit(integer)), integer/10);
}

template<size_t N, typename T>
constexpr HeadPushString<N> UIntToString(const T & integer) {
    return integer < 0 ? throw std::invalid_argument("integer less than 0") : UIntToString(HeadPushString<N>(), integer);
}

template<size_t N>
constexpr HeadPushString<N> GetFileBasename(const HeadPushString<N> & string, const char (&filename)[N], size_t index) {
    return index == 0 ? string.PushCharFront(filename[index]) :
           filename[index] == '/' || filename[index] == '\\' ? string : 
           GetFileBasename(string.PushCharFront(filename[index]), filename, index - 1);
}

template<size_t N>
constexpr HeadPushString<N> GetFileBasename(const char (&filename)[N]) {
    return GetFileBasename(HeadPushString<N>(), filename, N-2);
}

template <size_t N, size_t M, size_t O>
constexpr constexpr_sha256::Sha256Digest CryptoAssertGetHash(
    const constexpr_sha256::HmacKey & key,
    const detail::HeadPushString<N> & line,
    const detail::HeadPushString<M> & filename,
    const char (&assertion)[O]) {
    using namespace constexpr_sha256;
    return Sha256_HMAC_Final(
        Sha256_HMAC_Update(
            Sha256_HMAC_Update(
                Sha256_HMAC_Update(
                    Sha256_HMAC_Update(
                        Sha256_HMAC_Update(
                            Sha256_HMAC_Init(key),
                            line.string, line.strlen
                        ),
                        " "
                    ),
                    filename.string, filename.strlen
                ),
                " "
            ),
            assertion
        )
    );
}

template <typename... T>
struct ConcatStringsImpl { };

template <typename... T>
struct ConcatStringsImpl<const char *, T...> {
    static std::string ConcatStrings(const char * arg, T... args) {
        return std::string(arg) + " " + ConcatStringsImpl<T...>::ConcatStrings(args...);
    }
};

template <typename T1, typename... T>
struct ConcatStringsImpl<T1, T...> {
    static 
    typename std::enable_if<std::is_arithmetic<T1>::value, std::string>::type 
    ConcatStrings(T1 arg, T... args){
        std::ostringstream ss;
        ss << arg;
        return ss.str() + " " + ConcatStringsImpl<T...>::ConcatStrings(args...);
    }
};

template <>
struct ConcatStringsImpl<> {
    static std::string ConcatStrings() {
        return "";
    }
};

}

// To generate a random key, you can use the following command:
// < /dev/urandom tr -dc a-f0-9 | head -c128 - | xargs python -c "import sys; a=sys.argv[1]; print(\", \".join([\"0x\" + a[i*2] + a[i*2+1] for i in range(len(a)/2)]));" | fold -s -w 48
constexpr uint8_t CRYPTO_ASSERT_NULL_KEY_RAW[64] = { 0 };
constexpr constexpr_sha256::HmacKey CRYPTO_ASSERT_NULL_KEY = 
    constexpr_sha256::HmacKey::Construct(CRYPTO_ASSERT_NULL_KEY_RAW, sizeof(CRYPTO_ASSERT_NULL_KEY_RAW));

// Result is the HMAC of "LINE FILENAME ASSERTION"
// You can use the following command to compute the result of this function:
// Replace <LINE FILENAME ASSERTION> with the input values, and replace <HMAC KEY> with your HMAC 
// key, encoded like \x00\x00\x00... for all 64 bytes.
// echo -n "<LINE FILENAME ASSERTION>" | openssl dgst -sha256 -hmac "`echo -e "<HMAC KEY>"`"
template <size_t N, size_t M>
constexpr constexpr_sha256::Sha256Digest CryptoAssertGetHash(
    const constexpr_sha256::HmacKey & key,
    const uint64_t & line,
    const char (&filename)[N],
    const char (&assertion)[M]) {
    return CryptoAssertGetHash(key, detail::UIntToString<22>(line), detail::GetFileBasename(filename), assertion);
}

template <size_t N, size_t M>
constexpr constexpr_sha256::helper::HexString<32> CryptoAssertGetHashString(
    const constexpr_sha256::HmacKey & key,
    const uint64_t & line,
    const char (&filename)[N],
    const char (&assertion)[M]) {
    return constexpr_sha256::helper::MakeHexString(CryptoAssertGetHash(key, line, filename, assertion).digest);
}

template <typename... T>
std::string ConcatStrings(T... args) {
    return detail::ConcatStringsImpl<T...>::ConcatStrings(args...);
}

}  // namespace crypto_assert

#endif  // CRYPTO_ASSERT_H

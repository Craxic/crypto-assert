#!/usr/bin/python3
# Crypto assert: cryptographically based assertions with compile time SHA-256
# using C++11 constexpr.
#
# Copyright (c) 2016 Matthew Ready
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
import os
import argparse
import json
import hashlib
import hmac
import re


CPP_LINE_COMMENT_REGEX = re.compile('//.*?(?:\n|$)')
CPP_BLOCK_COMMENT_REGEX = re.compile('/\*(?:.|\n)*?\*/')
CPP_WHITESPACE_REGEX = re.compile('\s+')


def cpp_macro_stringify(code):
    # https://gcc.gnu.org/onlinedocs/cpp/Stringification.html
    #    - Trim leading and trailing whitespace.
    #    - Remove any comments
    #    - Any consecutive whitespace becomes a single space.
    code = CPP_LINE_COMMENT_REGEX.sub(" ", code)
    code = CPP_BLOCK_COMMENT_REGEX.sub(" ", code)
    code = CPP_WHITESPACE_REGEX.sub(" ", code)
    code = code.strip()
    return code


def count_backslashes_before(source, index):
    start_index = index
    while index > 0 and source[index] == '\\':
        index -= 1
    return start_index - index


def get_index_of_string_ender(source):
    for i in range(1, len(source)):
        if source[i] == "\"":
            # Count consecutive backslashes before this slash. If its even,
            # then end the string.
            if count_backslashes_before(source, i-1) % 2 == 0:
                return i
    return len(source)


def calculate_hmac(hmac_plain, key):
    return hmac.new(key, hmac_plain, hashlib.sha256).hexdigest()


def make_assertion(source, basename, macro_start, macro_end, firstarg, key):
    # CPP Macro Stringify firstarg
    firstarg = cpp_macro_stringify(firstarg)
    # Skip assertion definition
    try:
        start_of_line = source.rindex("\n", 0, macro_start) + 1
    except ValueError:
        start_of_line = 0
    if source[start_of_line] == "#":
        return None
    line_number = source.count("\n", 0, macro_end) + 1
    hmac_plain = str(line_number) + " " + basename + " " + firstarg

    return {
        "basename": basename,
        "line_number": line_number,
        "assertion_expression": firstarg,
        "hmac_plain_text": hmac_plain,
        "hmac": calculate_hmac(hmac_plain.encode(), key),
    }


def scan_source(source, basename, assertion_macros, key):
    assertion_macro_regexes = [re.compile(re.escape(x) + "\\s*\\(")
                               for x in assertion_macros]

    current_macro_with_bracket = None
    assertions = []
    macro_start_index = -1
    end_first_arg_index = -1
    bracket_depth = -1
    i = 0
    while i < len(source):
        tail = source[i:]
        if tail.startswith("/*"):
            # Skip to */
            try:
                i += tail.index("*/") + 2
            except ValueError:
                raise ValueError("Block comment is not terminated.")
        elif tail.startswith("R\""):
            # Read raw string starter.
            try:
                starter_end = tail.index("(")
                starter = tail[2:starter_end]
                i += tail.index(")" + starter + "\"") + 2 + len(starter)
            except ValueError:
                raise ValueError("Raw string is not terminated.")
        elif tail.startswith("\""):
            # Skip to first " that isn't preceded with a backslash
            i += get_index_of_string_ender(tail) + 1
        elif tail.startswith("//"):
            try:
                i += tail.index("\n")
            except ValueError:
                i += len(tail)
        elif tail.startswith("("):
            # Adjust bracket depth, skip character
            bracket_depth += 1
            i += 1
        elif tail.startswith(")"):
            bracket_depth -= 1
            i += 1
            # Adjust bracket depth, skip character
            if macro_start_index >= 0 and bracket_depth == 0:
                if end_first_arg_index < 0:
                    end_first_arg_index = i

                first_arg_start = macro_start_index + len(current_macro_with_bracket)
                first_arg_end = end_first_arg_index - 1
                first_arg = source[first_arg_start:first_arg_end]

                assertion = make_assertion(source, basename,
                                           macro_start_index, i, first_arg, key)
                if assertion is not None:
                    assertions.append(assertion)
                macro_start_index = -1
                end_first_arg_index = -1
                current_macro_with_bracket = None
                pass
        elif tail.startswith(","):
            i += 1
            # Set end of first arg.
            if macro_start_index >= 0 and bracket_depth == 1 \
                    and end_first_arg_index < 0:
                end_first_arg_index = i
        else:
            for assertion_macro_regex in assertion_macro_regexes:
                match = assertion_macro_regex.match(tail)
                if match is not None:
                    assertion_macro_with_bracket = match.group(0)
                    # Set bracket depth to 0, now in macro.
                    macro_start_index = i
                    end_first_arg_index = -1
                    bracket_depth = 1
                    i += len(assertion_macro_with_bracket)
                    current_macro_with_bracket = assertion_macro_with_bracket
                    break
            else:
                i += 1
    return assertions


def scan_file(filename, assertion_macro, key_as_bytes):
    # Load file data
    with open(filename, "r") as file:
        file_source = file.read()
    # Pass to scan_source
    return scan_source(file_source, os.path.basename(filename),
                       assertion_macro, key_as_bytes)


def scan(directory, extensions, assertion_macro, key_as_bytes):
    located_assertions = []
    # Walk through all files in directory recursively.
    for dirpath, dirnames, filenames in os.walk(directory, followlinks=True):
        for filename in filenames:
            # Check if the file has the right extension
            if any([filename.endswith("." + ext) for ext in extensions]):
                # It does: scan this file!
                located_assertions += \
                    scan_file(os.path.join(dirpath, filename), assertion_macro, key_as_bytes)
    return located_assertions


def main():
    """
    Entry point for index.py. Reads arguments, runs scan() and then writes
    returned value to a JSON file.
    :return: None
    """
    parser = argparse.ArgumentParser(
        description='creates a list that maps assertion hashes to their '
                    'plaintext')

    parser.add_argument('--assertion_macros', '-a',
                        dest='assertion_macros',
                        action='store',
                        default='CRYPTO_ASSERT',
                        help='comma separated list of macros to search for')

    parser.add_argument('--extensions', '-e',
                        dest='extensions',
                        action='store',
                        default='cpp,c++,cp,cxx,cc,hpp,h,hxx,hh,inl',
                        help='comma separated list of file extensions to scan')

    parser.add_argument('key',
                        action='store',
                        help='SHA256 HMAC Key')

    parser.add_argument('dir',
                        action='store',
                        default='.',
                        help='directory to source files')

    parser.add_argument('outfile',
                        action='store',
                        default='crypto_assert.json',
                        help='output json file, - for stdout')

    args = parser.parse_args()

    key_as_bytes = bytes.fromhex(args.key)
    assertion_macros = args.assertion_macros.split(",")
    if len(set(assertion_macros)) != len(assertion_macros):
        print("Assertion macros contains duplicates. You probably don't want "
              "this, bailing.")
        return 1
    result = scan(args.dir, args.extensions.split(","), assertion_macros,
                  key_as_bytes)
    result_json = json.dumps(result, indent=4)
    if args.outfile == '-':
        print(result_json)
    else:
        with open(args.outfile, 'w') as outfile:
            outfile.write(result_json)
    return 0

if __name__ == '__main__':
    exit(main())
